# My JavaScripts

Learn Javascript

Button On Click -

1. Change html content
   like, onclick="document.getElementById('demo').innerHTML = 'Hello JavaScript' "
2. Change HTML Attribute Values
   Eg, <button onclick="document.getElementById('myImage').src='pic_bulbon.gif'">Turn on the light</button>
   Change the source of image
3. Change HTML styles
   Eg, document.getElementById("demo").style.fontSize = "35px"
4. Hide HTML Elements
   Eg, document.getElementById("demo").style.display = "none"
5. Show HTML Elements
   Eg, document.getElementById("demo").style.display = "block"
   
JavaScript code must be inserted between <script> and </script> tags.
A JavaScript function is a block of JavaScript code, that can be executed when "called" for.
Scripts can be placed in the <body>, or in the <head> section of an HTML page, or in both.
Scripts can also be placed in external files: JavaScript files have the file extension .js.
	To use an external script, put the name of the script file in the src (source) attribute of a <script> tag:
		<script src="myScript.js"></script>
		<script src="/js/myScript1.js"></script>
		
		External JavaScript Advantages
			Placing scripts in external files has some advantages:

				It separates HTML and code
				It makes HTML and JavaScript easier to read and maintain
				Cached JavaScript files can speed up page loads
JavaScript can "display" data in different ways:

	Writing into an HTML element, using innerHTML.
	Writing into the HTML output using document.write().
	Writing into an alert box, using window.alert().
	Writing into the browser console, using console.log().
Using document.write() after an HTML document is loaded, will delete all existing HTML: The document.write() method should only be used for testing.
For debugging purposes, you can use the console.log() method to display data.
It writes on console

JavaScript Statements
JavaScript programs (and JavaScript statements) are often called JavaScript code.
Semicolons separate JavaScript statements.
Add a semicolon at the end of each executable statement:
var a, b, c;     // Declare 3 variables
a = 5;           // Assign the value 5 to a
b = 6;           // Assign the value 6 to b
c = a + b;       // Assign the sum of a and b to c
a = 5; b = 6; c = a + b;

JavaScript ignores multiple spaces. You can add white space to your script to make it more readable.

JavaScript Keywords

JavaScript statements often start with a keyword to identify the JavaScript action to be performed.

break	        Terminates a switch or a loop
continue	    Jumps out of a loop and starts at the top
debugger	    Stops the execution of JavaScript, and calls (if available) the debugging function
do ... while	Executes a block of statements, and repeats the block, while a condition is true
for	            Marks a block of statements to be executed, as long as a condition is true
function	    Declares a function
if ... else	    Marks a block of statements to be executed, depending on a condition
return	        Exits a function
switch	        Marks a block of statements to be executed, depending on different cases
try ... catch	Implements error handling to a block of statements
var	            Declares a variable


JavaScript keywords are reserved words. Reserved words cannot be used as names for variables.

The JavaScript syntax defines two types of values: Fixed values and variable values.
Fixed values are called literals. 
Variable values are called variables.

document.getElementById("demo").innerHTML = "John" + " "  + "Doe";

Code after double slashes // or between /* and */ is treated as a comment.

JavaScript Identifiers

In JavaScript, identifiers are used to name variables (and keywords, and functions, and labels).
In JavaScript, the first character must be a letter, or an underscore (_), or a dollar sign ($).
All JavaScript identifiers are case sensitive. 

<script>
var lastname, lastName;
lastName = "Doe";
lastname = "Peterson";
document.getElementById("demo").innerHTML = lastName;
</script>

Hyphens are not allowed in JavaScript. They are reserved for subtractions.
Lower Camel Case:

JavaScript programmers tend to use camel case that starts with a lowercase letter:
firstName, lastName, masterCard, interCity.

If you re-declare a JavaScript variable, it will not lose its value.

x ** y produces the same result as Math.pow(x,y):
Multiplication (*) and division (/) have higher precedence than addition (+) and subtraction (-).
When adding a number and a string, JavaScript will treat the number as a string.
Booleans can only have two values: true or false.

var x = 5;
var y = 5;
var z = 6;
(x == y)       // Returns true
(x == z)       // Returns false

JavaScript arrays are written with square brackets.

var cars = ["Saab", "Volvo", "BMW"];

JavaScript objects are written with curly braces {}.

var person = {firstName:"John", lastName:"Doe", age:50, eyeColor:"blue"};

The typeof Operator
You can use the JavaScript typeof operator to find the type of a JavaScript variable.

Any variable can be emptied, by setting the value to undefined. The type will also be undefined.

You can empty an object by setting it to null:

var person = {firstName:"John", lastName:"Doe", age:50, eyeColor:"blue"};
person = null;    // Now value is null, but type is still an object

typeof undefined           // undefined
typeof null                // object

null === undefined         // false
null == undefined          // true

typeof {name:'John', age:34} // Returns "object"
typeof [1,2,3,4]             // Returns "object" (not "array", see note below)
typeof null                  // Returns "object"
typeof function myFunc(){}   // Returns "function"

JavaScript Functions

Variables declared within a JavaScript function, become LOCAL to the function.

Objects can also have methods.
Methods are actions that can be performed on objects.
Methods are stored in properties as function definitions.

var person = {
  firstName: "John",
  lastName : "Doe",
  id       : 5566,
  fullName : function() {
    return this.firstName + " " + this.lastName;
  }
};

In a function definition, this refers to the "owner" of the function.

When a JavaScript variable is declared with the keyword "new", the variable is created as an object:

var x = new String();        // Declares x as a String object
var y = new Number();        // Declares y as a Number object
var z = new Boolean();       // Declares z as a Boolean object


HTML Events
An HTML event can be something the browser does, or something a user does.
JavaScript lets you execute code when events are detected.

HTML allows event handler attributes, with JavaScript code, to be added to HTML elements.

With single quotes:

<element event='some JavaScript'>
With double quotes:

<element event="some JavaScript">

<button onclick="document.getElementById('demo').innerHTML = Date()">The time is?</button>

Common HTML Events
onchange	An HTML element has been changed
onclick	The user clicks an HTML element
onmouseover	The user moves the mouse over an HTML element
onmouseout	The user moves the mouse away from an HTML element
onkeydown	The user pushes a keyboard key
onload	The browser has finished loading the page

JS String

To find the length of a string, use the built-in length property:

var txt = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
var sln = txt.length;

Escape Character
var x = "We are the so-called "Vikings" from the north.";
The string will be chopped to "We are the so-called ".
The solution to avoid this problem, is to use the backslash escape character.
The backslash (\) escape character turns special characters into string characters:

\b	Backspace
\f	Form Feed
\n	New Line
\r	Carriage Return
\t	Horizontal Tabulator
\v	Vertical Tabulator

Strings Can be Objects

var x = "John";
var y = new String("John");

// typeof x will return string
// typeof y will return object